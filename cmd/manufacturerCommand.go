package cmd

import (
	"net"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"gitlab.com/vowo/vowo-manufacturers/services/manufacturers"
	"go.uber.org/zap"
)

// manufacturersCommand represents the manufacturers command
var manufacturersCommand = &cobra.Command{
	Use:   "manufacturers",
	Short: "Starts manufacturers service",
	Long:  `Starts manufacturers service.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		zapLogger := logger.With(zap.String("service", "manufacturers"))
		logger := log.NewFactory(zapLogger)
		server := manufacturers.NewServer(
			net.JoinHostPort(manufacturersOptions.serverInterface, strconv.Itoa(manufacturersOptions.serverPort)),
			net.JoinHostPort(manufacturersOptions.serverInterface, strconv.Itoa(manufacturersOptions.serverHealthPort)),
			tracing.Init("manufacturers", metricsFactory.Namespace("manufacturers", nil), logger, jAgentHostPort),
			metricsFactory,
			logger,
			jAgentHostPort,
		)

		go server.RunHealthAndReadinessProbes()
		return logError(zapLogger, server.Run())
	},
}

var (
	manufacturersOptions struct {
		serverInterface  string
		serverPort       int
		serverHealthPort int
	}
)

func init() {
	RootCmd.AddCommand(manufacturersCommand)

	//TODO read port from env
	manufacturersCommand.Flags().StringVarP(&manufacturersOptions.serverInterface, "bind", "", "0.0.0.0", "interface to which the manufacturers server will bind")
	manufacturersCommand.Flags().IntVarP(&manufacturersOptions.serverPort, "port", "p", 8083, "port on which the manufacturers server will listen")
	manufacturersCommand.Flags().IntVarP(&manufacturersOptions.serverHealthPort, "healthPort", "e", 8183, "port on which the health check of the manufacturers service will listen")
}
