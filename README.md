# vowo.io Manufacturers

Microservice providing information about manufacturers for vowo.io

[![pipeline status](https://gitlab.com/vowo/vowo-manufacturers/badges/master/pipeline.svg)](https://gitlab.com/vowo/vowo-manufacturers/commits/master)

## Run it

```bash
dep ensure
docker run -d -p6831:6831/udp -p16686:16686 jaegertracing/all-in-one:latest
go build -v -o ./output/vowo . && ./output/vowo manufacturers
```
