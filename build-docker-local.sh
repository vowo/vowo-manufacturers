#!/bin/bash
set -e

if [ $# -eq 1 ]
then
    IMAGE_TAG=$1
else
    IMAGE_TAG=$(basename $(pwd))
fi

set -x
docker build . -t $IMAGE_TAG