# build environment
FROM golang:1.10.3-alpine3.8 as builder
RUN apk update && apk add curl git gcc musl-dev libtool
WORKDIR /go/src/gitlab.com/vowo/vowo-manufacturers

# Go dependencies
RUN curl -s https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
COPY Gopkg.toml Gopkg.lock ./

RUN dep ensure --vendor-only
# App
COPY . .

# go to folder and build golang app as static binary enabling cgo related from the previous dependencies mentioned
RUN CGO_ENABLED=1 GOOS=linux go build -a -tags netgo -o /out/app

# deploy environment
FROM alpine:latest as alpine
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /out/* .

EXPOSE 8083 8183
ENTRYPOINT ["/app/app", "manufacturers"]