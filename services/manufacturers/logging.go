package manufacturers

import (
	"context"
	"time"

	"github.com/go-kit/kit/endpoint"
	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"go.uber.org/zap"
)

type loggingService struct {
	tracer opentracing.Tracer
	logger log.Factory
	Service
}

//LoggingMiddleware middleware to log requests and duration
func LoggingMiddleware(logger log.Factory) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {

			defer func(begin time.Time) {
				if err != nil {
					logger.For(ctx).Error("transport_error", zap.Error(err), zap.Stringer("took", time.Since(begin)))
				} else {
					logger.For(ctx).Info("request completed", zap.Any("request", request), zap.Any("response", response), zap.Stringer("took", time.Since(begin)))
				}

			}(time.Now())
			return next(ctx, request)
		}
	}
}
