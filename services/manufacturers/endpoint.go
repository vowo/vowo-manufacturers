package manufacturers

import (
	"context"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// manufacturersRequest for an http request
type manufacturersRequest struct {
}

// manufacturersResponse to an http request
type manufacturersResponse struct {
	Manufacturers []*Manufacturer `json:"manufacturers,omitempty"`
	Err           error           `json:"-"` // should be intercepted by Failed/errorEncoder
}

// getByIDRequest for an http request
type getByIDRequest struct {
	id uuid.UUID
}

// getByIDResponse to an http request
type getByIDResponse struct {
	Manufacturer *Manufacturer `json:"manufacturer,omitempty"`
	Err          error         `json:"-"` // should be intercepted by Failed/errorEncoder
}

// Set collects all of the endpoints that compose an manufacturers service.
type Set struct {
	findAll endpoint.Endpoint
	getByID endpoint.Endpoint
}

// GetAllManufacturers implements the manufacturers.Service
func (s *Set) GetAllManufacturers(ctx context.Context) ([]*Manufacturer, error) {
	resp, err := s.findAll(ctx, manufacturersRequest{})
	if err != nil {
		return nil, err
	}

	response := resp.(manufacturersResponse)
	return response.Manufacturers, response.Err
}

// GetManufacturerByID implements the manufacturers.Service
func (s *Set) GetManufacturerByID(ctx context.Context, id uuid.UUID) (*Manufacturer, error) {
	resp, err := s.getByID(ctx, getByIDRequest{id: id})
	if err != nil {
		return nil, err
	}

	response := resp.(getByIDResponse)
	return response.Manufacturer, response.Err
}

// NewEndpoint creates a new manufacturers endpoint
func NewEndpoint(s Service, tracer opentracing.Tracer, logger log.Factory) *Set {
	var findAll endpoint.Endpoint
	{
		findAll = makeFindAllEndpoint(s)
		findAll = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(findAll)
		findAll = kitopentracing.TraceServer(tracer, "manufacturers")(findAll)
		findAll = LoggingMiddleware(logger)(findAll)
	}

	var getByID endpoint.Endpoint
	{
		getByID = makeGetByIDEndpoint(s)
		getByID = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(getByID)
		getByID = kitopentracing.TraceServer(tracer, "manufacturers")(getByID)
		getByID = LoggingMiddleware(logger)(getByID)
	}

	return &Set{
		findAll: findAll,
		getByID: getByID,
	}
}

func makeFindAllEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		manufacturers, err := s.GetAllManufacturers(ctx)

		if err != nil {
			return nil, err
		}

		return &manufacturersResponse{Manufacturers: manufacturers, Err: err}, nil
	}
}

func makeGetByIDEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getByIDRequest)
		manufacturer, err := s.GetManufacturerByID(ctx, req.id)

		if err != nil {
			return nil, err
		}

		return &getByIDResponse{Manufacturer: manufacturer, Err: err}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = manufacturersResponse{}
)

// Failed implements endpoint.Failer.
func (r manufacturersResponse) Failed() error { return r.Err }
