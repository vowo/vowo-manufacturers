package manufacturers

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"

	"github.com/satori/go.uuid"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/vowo/vowo-corelib/pkg/httperr"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// MakeHandler returns a handler for the manufacturers service.
func (s *Server) MakeHandler(endpoints *Set) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(s.EncodeError),
	}

	findAllHandler := kithttp.NewServer(
		endpoints.findAll,
		decodeHTTPGenericRequest,
		httputils.EncodeHTTPGenericResponse,
		append(opts, kithttp.ServerBefore(tracing.HTTPToContext(s.tracer, "manufacturers", s.logger)))...,
	)

	getByIDHandler := kithttp.NewServer(
		endpoints.getByID,
		decodeHTTPGetByIDRequest,
		httputils.EncodeHTTPGenericResponse,
		append(opts, kithttp.ServerBefore(tracing.HTTPToContext(s.tracer, "getManufacturerByID", s.logger)))...,
	)

	r := mux.NewRouter()
	r.Handle("/", findAllHandler).Methods("GET")
	r.Handle("/{traceID}", getByIDHandler).Methods("GET")
	r.NotFoundHandler = http.HandlerFunc(httperr.HandleNotFound)
	r.MethodNotAllowedHandler = http.HandlerFunc(httperr.HandleMethodNotAllowed)

	return r
}

func decodeHTTPGetByIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, ok := mux.Vars(r)["traceID"]
	if !ok || id == "" {
		return nil, errInvalidArgument
	}

	traceUUID, err := uuid.FromString(id)
	if err != nil {
		return nil, errors.Wrap(errInvalidArgument, err.Error())
	}

	return getByIDRequest{id: traceUUID}, nil
}

func decodeHTTPGenericRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeHTTPGenericRequest(ctx context.Context, r *http.Request, request interface{}) error {
	return nil
}

func encodeGetByIDRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getByIDRequest)
	r.URL.Path = "/" + req.id.String()
	return nil
}

func decodeGetByIDResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}

	var resp getByIDResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

func decodeHTTPFindAllResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}

	var resp manufacturersResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

// EncodeError encode errors from business-logic
func (s *Server) EncodeError(ctx context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	innerErr := errors.Cause(err)
	if innerErr == nil {
		innerErr = err
	}

	switch innerErr {
	case errInvalidArgument:
		s.logger.For(ctx).Info("bad request", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
	case errNotFound:
		s.logger.For(ctx).Info("not found", zap.Error(err))
		w.WriteHeader(http.StatusNotFound)
	default:
		s.logger.For(ctx).Fatal("server error", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
