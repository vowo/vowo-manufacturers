package manufacturers

import "errors"

var errInvalidArgument = errors.New("invalid argument")
var errNotFound = errors.New("not found")
