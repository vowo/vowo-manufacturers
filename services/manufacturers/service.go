package manufacturers

import (
	"context"

	"github.com/satori/go.uuid"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// Service is the interface to access manufacturers.
type Service interface {
	GetAllManufacturers(ctx context.Context) ([]*Manufacturer, error)
	GetManufacturerByID(ctx context.Context, id uuid.UUID) (*Manufacturer, error)
}

type service struct {
	tracer                  opentracing.Tracer
	logger                  log.Factory
	manufacturersRepository Repository
}

func (s *service) GetAllManufacturers(ctx context.Context) ([]*Manufacturer, error) {
	manufacturers := s.manufacturersRepository.FindAll(ctx)
	return manufacturers, nil
}

func (s *service) GetManufacturerByID(ctx context.Context, id uuid.UUID) (*Manufacturer, error) {
	return s.manufacturersRepository.GetByID(ctx, id)
}

// NewService creates an manufacturers service with necessary dependencies.
func NewService(tracer opentracing.Tracer, logger log.Factory, manufacturersRepository Repository) Service {
	return &service{
		logger:                  logger,
		tracer:                  tracer,
		manufacturersRepository: manufacturersRepository,
	}
}
