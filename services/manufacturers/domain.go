package manufacturers

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

// Repository provides access the manufacturers store.
type Repository interface {
	FindAll(ctx context.Context) []*Manufacturer
	GetByID(ctx context.Context, id uuid.UUID) (*Manufacturer, error)
}

// Manufacturer domain model
type Manufacturer struct {
	Name       string
	ID         uuid.UUID
	Location   *Coord
	WebsiteURL string
}

// Coord represents a geographic coordinate.
type Coord struct {
	Lat float64
	Lon float64
}
