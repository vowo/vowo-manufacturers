package manufacturers

import (
	"net/url"
	"time"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
)

// NewHTTPClient creates a new manufacturers.Service backed by an HTTP server living at the
// remote instance. Instance is expected to come from a service discovery system,
// so likely of the form "host:port".
func NewHTTPClient(instance string, tracer opentracing.Tracer, logger log.Factory) (Service, error) {
	instance = httputils.HTTPPrefixURL(instance)
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	options := []httptransport.ClientOption{}
	var findAllEndpoint endpoint.Endpoint
	{
		findAllEndpoint = httptransport.NewClient(
			"GET",
			httputils.AddPathToURL(u, ""),
			encodeHTTPGenericRequest,
			decodeHTTPFindAllResponse,
			append(options, httptransport.ClientBefore(tracing.ContextToHTTP(tracer, logger)))...,
		).Endpoint()
		findAllEndpoint = kitopentracing.TraceClient(tracer, "manufacturers")(findAllEndpoint)
		findAllEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "manufacturers",
			Timeout: 30 * time.Second,
		}))(findAllEndpoint)
	}

	var getByIDEndpoint endpoint.Endpoint
	{
		getByIDEndpoint = httptransport.NewClient(
			"GET",
			httputils.AddPathToURL(u, ""),
			encodeGetByIDRequest,
			decodeGetByIDResponse,
			append(options, httptransport.ClientBefore(tracing.ContextToHTTP(tracer, logger)))...,
		).Endpoint()
		getByIDEndpoint = kitopentracing.TraceClient(tracer, "manufacturers")(getByIDEndpoint)
		getByIDEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "manufacturers",
			Timeout: 30 * time.Second,
		}))(getByIDEndpoint)
	}

	return &Set{
		findAll: findAllEndpoint,
		getByID: getByIDEndpoint,
	}, nil
}
