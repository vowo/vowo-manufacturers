package manufacturers

import (
	"context"
	"fmt"
	"sync"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

type manufacturersRepository struct {
	mtx           sync.RWMutex
	manufacturers map[uuid.UUID]*Manufacturer
	tracer        opentracing.Tracer
	logger        log.Factory
}

func (r *manufacturersRepository) FindAll(ctx context.Context) []*Manufacturer {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	allmanufacturers := make([]*Manufacturer, 0, len(r.manufacturers))
	for _, manufacturer := range r.manufacturers {
		allmanufacturers = append(allmanufacturers, manufacturer)
	}

	return allmanufacturers
}

func (r *manufacturersRepository) GetByID(ctx context.Context, id uuid.UUID) (*Manufacturer, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	if val, ok := r.manufacturers[id]; ok {
		return val, nil
	}

	return nil, errors.Wrap(errNotFound, fmt.Sprintf("manufacturer with id %v", id))
}

// newmanufacturersRepository returns a new instance of a in-memory manufacturers repository.
func newmanufacturersRepository(tracer opentracing.Tracer, logger log.Factory) *manufacturersRepository {
	maufacturerID1, _ := uuid.FromString("4630d109-c8f7-4fa4-9e67-2deab9c40109")
	maufacturerID2, _ := uuid.FromString("49bf9ffc-d359-42db-b094-d56ba4b3c233")
	maufacturerID3, _ := uuid.FromString("6935f5ec-5ffb-402e-b076-aeffaeb3e066")
	return &manufacturersRepository{
		tracer: tracer,
		logger: logger,
		manufacturers: map[uuid.UUID]*Manufacturer{
			maufacturerID1: &Manufacturer{
				Name: "Bio-Hof Rotstalden",
				ID:   maufacturerID1,
				Location: &Coord{
					Lat: 47.0569559,
					Lon: 7.7406874,
				},
				WebsiteURL: "http://rotstalden.ch",
			},
			maufacturerID2: &Manufacturer{
				Name: "Sigi's Biohof Schwand",
				ID:   maufacturerID2,
				Location: &Coord{
					Lat: 46.890245,
					Lon: 7.554844,
				},
				WebsiteURL: "http://sigis-biohof.ch/wp/",
			},
			maufacturerID3: &Manufacturer{
				Name: "Winkelmann Hansueli",
				ID:   maufacturerID3,
				Location: &Coord{
					Lat: 46.796852,
					Lon: 7.386071,
				},
				WebsiteURL: "https://www.eiermeier.ch/produzent-nummer-3039",
			},
		},
	}
}
